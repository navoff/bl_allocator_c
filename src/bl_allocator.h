/**
 * @file bl_alloc.h
 * @author Vladimir Postnov
 * @date 21.07.2018
 * @brief
 * @addtogroup
 * @{
*/

#ifndef BL_ALLOCATOR_H_
#define BL_ALLOCATOR_H_

#include <stdbool.h>
#include <stdint.h>

// ======================================= Definition ==============================================

/// Size of the pool in chunks
#define BL_POOL_SIZE                10u

/// Size of the single chunk in bytes
#define BL_CHUNK_SIZE               15u

struct bl_chunk_s
{
  /// Pointer to the next free chunk
  struct bl_chunk_s *next_free_chunk;

  /// User buffer
  uint8_t buffer[BL_CHUNK_SIZE];
};
typedef struct bl_chunk_s bl_chunk_t;

typedef struct
{
  /// Poiter to the first free chunk in the pool
  /// @note If equal NULL - out of memory
  bl_chunk_t *first_free_chunk;

  /// Array of all chunks in the pool
  bl_chunk_t chunks[BL_POOL_SIZE];
} bl_allocator_t;

// ======================================= Declaration =============================================

/**
 * @brief Initialize allocator
 * @param bl_allocator - allocator instance
 * @note Must be call before using other API
 */
void bl_init(bl_allocator_t *bl_allocator);

/**
 * @brief Reset pool and release all chunks
 * @param bl_allocator - allocator instance
 */
void bl_reset(bl_allocator_t *bl_allocator);

/**
 * @brief Allocate one chunk
 * @param bl_allocator - allocator instance
 * @return Pointer to the allocated buffer
 *         NULL - if is out of memory
 */
void *bl_allocate(bl_allocator_t *bl_allocator);

/**
 * @brief Deallocate chunk
 * @param bl_allocator - allocator instance
 * @param buffer - pointer to release buffer
 */
void bl_deallocate(bl_allocator_t *bl_allocator, void *buffer);

#endif /* BL_ALLOCATOR_H_ */

/** @} */
