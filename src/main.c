/**
 * @file main.c
 * @author Vladimir Postnov
 * @date 21.07.2018
 * @brief
 * @addtogroup
 * @{
*/

#include <stdio.h>
#include <stdlib.h>

#include "bl_allocator_test.h"

// ======================================= Definition ==============================================

// ======================================= Declaration =============================================

void bl_test(void);

// ====================================== Implementation ===========================================

int main(void)
{
  bl_test();
}

void bl_test(void)
{
  bl_allocator_t bl;
  bl_init(&bl);

  printf("test chunk amount:             %s\n", bl_test_chunk_amount(&bl) ? "success" : "fail");

  bl_reset(&bl);
  printf("test chunk amount after reset: %s\n", bl_test_chunk_amount(&bl) ? "success" : "fail");

  bl_reset(&bl);
  printf("test alloc:                    %s\n", bl_test_alloc(&bl) ? "success" : "fail");

  bl_reset(&bl);
  printf("test alloc and free:           %s\n", bl_test_alloc_and_free(&bl) ? "success" : "fail");
}
/** @} */
