/**
 * @file bl_allocator_test.h
 * @author Постнов В.М.
 * @date 20 янв. 2020 г.
 * @brief
 * @addtogroup
 * @{
*/

#ifndef BL_ALLOCATOR_TEST_H_
#define BL_ALLOCATOR_TEST_H_

#include <stdbool.h>

#include "bl_allocator.h"

// ======================================= Definition ==============================================


// ======================================= Declaration =============================================

/**
 * @brief Checking capacity of the pool
 * @param bl_allocator - allocator instance
 * @return true - test is succeed, false - test is failed
 * @note Must be call right after init or reset pool
 *
 * Get the total number of the chunk in pool (by allocatin all free space)
 * And compare this number with @ref BL_POOL_SIZE
 */
bool bl_test_chunk_amount(bl_allocator_t *bl_allocator);

/**
 * @brief Checking allocate the all pool
 * @param bl_allocator - allocator instance
 * @return true - test is succeed, false - test is failed
 * @note Must be call right after init or reset pool
 *
 * Allocate and fill all buffers by a sample
 * And then compare buffers with a sample from begin
 */
bool bl_test_alloc(bl_allocator_t *bl_allocator);

/**
 * @brief Simple checking allocate and free
 * @param bl_allocator - allocator instance
 * @return true - test is succeed, false - test is failed
 * @note Must be call right after init or reset pool
 *
 * Check sequence:
 * - allocate and fill all chunks by a sample
 * - free only odd chunks
 * - allocate freed chunks again and fill it by another sample
 * - compare all chunks with a sample
 */
bool bl_test_alloc_and_free(bl_allocator_t *bl_allocator);

#endif /* BL_ALLOCATOR_TEST_H_ */

/** @} */
