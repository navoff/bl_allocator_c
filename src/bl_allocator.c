/**
 * @file bl_alloc.c
 * @author Vladimir Postnov
 * @date 21.07.2018
 * @brief
 * @addtogroup
 * @{
*/

#include "bl_allocator.h"

#include <stddef.h>

// ======================================= Definition ==============================================

/**
 * @defgroup BL_CRITICAL
 * @brief Start and finish atomic operation
 *
 * Example with enable/disable irq for Cortex-M ARM MCU (or AVR) with IAR compiler:
 *
 *    #include "intrinsics.h"
 *
 *    ...
 *
 *    #define BL_ENTER_CRITICAL()      __istate_t s = __get_interrupt_state(); __disable_interrupt()
 *    #define BL_LEAVE_CRITICAL()      __set_interrupt_state(s)
 *
 * @{
 */
#define BL_ENTER_CRITICAL()       do{ }while(0)
#define BL_LEAVE_CRITICAL()       do{ }while(0)
/** @} */

// ======================================= Declaration =============================================

//bl_pool_t bl_pool;

// ====================================== Implementation ===========================================

void bl_init(bl_allocator_t *bl_allocator)
{
  for (size_t i = 0; i < BL_POOL_SIZE - 1; i++)
  {
    bl_allocator->chunks[i].next_free_chunk = &bl_allocator->chunks[i + 1];
  }
  bl_allocator->chunks[BL_POOL_SIZE - 1].next_free_chunk = NULL;

  bl_allocator->first_free_chunk = &bl_allocator->chunks[0];
}

void bl_reset(bl_allocator_t *bl_allocator)
{
  BL_ENTER_CRITICAL();

  bl_allocator->first_free_chunk = &bl_allocator->chunks[0];

  BL_LEAVE_CRITICAL();
}

void *bl_allocate(bl_allocator_t *bl_allocator)
{
  if (bl_allocator->first_free_chunk == NULL)
  {
    // out of memory
    return NULL;
  }

  BL_ENTER_CRITICAL();

  void *res = bl_allocator->first_free_chunk->buffer;
  bl_allocator->first_free_chunk = bl_allocator->first_free_chunk->next_free_chunk;

  BL_LEAVE_CRITICAL();

  return res;
}

void bl_deallocate(bl_allocator_t *bl_allocator, void *buffer)
{
  void *pool_begin_address = bl_allocator->chunks;
  void *pool_end_address = ((uint8_t *)bl_allocator->chunks) + sizeof(bl_allocator->chunks);

  if ((buffer < pool_begin_address) || (buffer >= pool_end_address))
  {
    // buf is outside the pool borders
    return;
  }

  BL_ENTER_CRITICAL();

  bl_chunk_t *chunk = (bl_chunk_t *)(((uint8_t *)buffer) - offsetof(bl_chunk_t, buffer));
  chunk->next_free_chunk = bl_allocator->first_free_chunk;
  bl_allocator->first_free_chunk = chunk;
  BL_LEAVE_CRITICAL();
}
/** @} */
